import Data.Char
import Data.List
numToArray :: Int -> [Int]
numToArray digits = map(\x -> read [x] :: Int)(show digits)

removeDuplicates :: (Eq a) => [a] -> [a]
removeDuplicates list = remDups list []

remDups ::(Eq a) => [a] -> [a] -> [a]
remDups [] _ = []
remDups (x:xs) list2
     | (x `elem` list2) = remDups xs list2
     | otherwise = x : remDups xs(x:list2)

setOfDigits :: Int -> [Int]
setOfDigits numbers = removeDuplicates (numToArray numbers)
    
isBeautiful :: Int -> Bool
isBeautiful year = (numToArray year == setOfDigits year)

nextBeautifulYear :: Int -> Int
nextBeautifulYear year = if isBeautiful (year + 1)  then year+1
                         else nextBeautifulYear (year + 1)

main = do
      print $ nextBeautifulYear 2000
