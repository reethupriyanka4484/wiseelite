import Data.Char
import Data.List
stringToChar :: String -> [String]
stringToChar chars = [[x] | x <- chars]

removeDuplicates :: (Eq a) => [a] -> [a]
removeDuplicates list = remDups list []

remDups :: (Eq a) => [a] -> [a] -> [a]
remDups [] _ = []
remDups (x:xs) list2
     | (x `elem` list2) = remDups xs list2
     | otherwise = x : remDups xs(x : list2)
isGirl :: String -> Bool
isGirl user = (length (removeDuplicates (stringToChar user))) `mod` 2 == 0
                                                                                                                                                     
main = do
      print $ if isGirl "reethu" then "ChatwithHer"
              else "Ignore Him"
